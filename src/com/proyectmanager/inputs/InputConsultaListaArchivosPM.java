package com.proyectmanager.inputs;

import com.proyectmanager.objects.Login;

public class InputConsultaListaArchivosPM {

    private Login Login;
    private String Id_consulta;
    private String Tipo_archivos;

    public InputConsultaListaArchivosPM() {
        super();
    }

    public void setLogin(Login Login) {
        this.Login = Login;
    }

    public Login getLogin() {
        return Login;
    }

    public void setId_consulta(String Id_consulta) {
        this.Id_consulta = Id_consulta;
    }

    public String getId_consulta() {
        return Id_consulta;
    }

    public void setTipo_archivos(String Tipo_archivos) {
        this.Tipo_archivos = Tipo_archivos;
    }

    public String getTipo_archivos() {
        return Tipo_archivos;
    }
}
