package com.proyectmanager.inputs;

import com.proyectmanager.objects.Login;

public class InputDownloadFileSalesforce {

    private Login Login;
    private String Id_file_sf;

    public InputDownloadFileSalesforce() {
        super();
    }

    public void setLogin(Login Login) {
        this.Login = Login;
    }

    public Login getLogin() {
        return Login;
    }

    public void setId_file_sf(String Id_file_sf) {
        this.Id_file_sf = Id_file_sf;
    }

    public String getId_file_sf() {
        return Id_file_sf;
    }


}
