package com.proyectmanager.inputs;

import com.proyectmanager.objects.Login;

public class InputCreaResponsableActividad {
    
    private Login Login;
    private String Nombre;
    private String Apellido_Paterno;
    private String Apellido_Materno;
    private String Telefono;
    
    public InputCreaResponsableActividad() {
        super();
    }

    public void setLogin(Login Login) {
        this.Login = Login;
    }

    public Login getLogin() {
        return Login;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setApellido_Paterno(String Apellido_Paterno) {
        this.Apellido_Paterno = Apellido_Paterno;
    }

    public String getApellido_Paterno() {
        return Apellido_Paterno;
    }

    public void setApellido_Materno(String Apellido_Materno) {
        this.Apellido_Materno = Apellido_Materno;
    }

    public String getApellido_Materno() {
        return Apellido_Materno;
    }

    public void setTelefono(String Telefono) {
        this.Telefono = Telefono;
    }

    public String getTelefono() {
        return Telefono;
    }
}
