package com.proyectmanager.inputs;

import com.proyectmanager.objects.Login;

public class InputInsertaComentariosPM {

    private Login Login;
    private String Comentario;
    private String Id_usuario;
    private String Id_cs;
    private String Id_actividad;

    public InputInsertaComentariosPM() {
        super();
    }


    public void setLogin(Login Login) {
        this.Login = Login;
    }

    public Login getLogin() {
        return Login;
    }

    public void setComentario(String Comentario) {
        this.Comentario = Comentario;
    }

    public String getComentario() {
        return Comentario;
    }

    public void setId_usuario(String Id_usuario) {
        this.Id_usuario = Id_usuario;
    }

    public String getId_usuario() {
        return Id_usuario;
    }

    public void setId_cs(String Id_cs) {
        this.Id_cs = Id_cs;
    }

    public String getId_cs() {
        return Id_cs;
    }

    public void setId_actividad(String Id_actividad) {
        this.Id_actividad = Id_actividad;
    }

    public String getId_actividad() {
        return Id_actividad;
    }
}
