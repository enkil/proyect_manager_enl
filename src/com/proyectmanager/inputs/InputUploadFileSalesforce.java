package com.proyectmanager.inputs;

import com.proyectmanager.objects.Login;

public class InputUploadFileSalesforce {

    private Login Login;
    private String Id_user_uploadFile;
    private String Id_tipo_imagen;
    private String Id_objeto_sf;
    private String Id_actividad;
    private String File_Base64;
    private String Nombre_archivo;
    private String Extencion_archivo;


    public InputUploadFileSalesforce() {
        super();
    }

    public void setLogin(Login Login) {
        this.Login = Login;
    }

    public Login getLogin() {
        return Login;
    }

    public void setId_user_uploadFile(String Id_user_uploadFile) {
        this.Id_user_uploadFile = Id_user_uploadFile;
    }

    public String getId_user_uploadFile() {
        return Id_user_uploadFile;
    }

    public void setId_tipo_imagen(String Id_tipo_imagen) {
        this.Id_tipo_imagen = Id_tipo_imagen;
    }

    public String getId_tipo_imagen() {
        return Id_tipo_imagen;
    }

    public void setId_objeto_sf(String Id_objeto_sf) {
        this.Id_objeto_sf = Id_objeto_sf;
    }

    public String getId_objeto_sf() {
        return Id_objeto_sf;
    }

    public void setId_actividad(String Id_actividad) {
        this.Id_actividad = Id_actividad;
    }

    public String getId_actividad() {
        return Id_actividad;
    }

    public void setFile_Base64(String File_Base64) {
        this.File_Base64 = File_Base64;
    }

    public String getFile_Base64() {
        return File_Base64;
    }

    public void setNombre_archivo(String Nombre_archivo) {
        this.Nombre_archivo = Nombre_archivo;
    }

    public String getNombre_archivo() {
        return Nombre_archivo;
    }

    public void setExtencion_archivo(String Extencion_archivo) {
        this.Extencion_archivo = Extencion_archivo;
    }

    public String getExtencion_archivo() {
        return Extencion_archivo;
    }
}
