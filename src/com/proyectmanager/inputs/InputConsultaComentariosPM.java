package com.proyectmanager.inputs;

import com.proyectmanager.objects.Login;

public class InputConsultaComentariosPM {

    private Login Login;
    private String Id_deconsulta;
    private String Tipo_comentarios;

    public InputConsultaComentariosPM() {
        super();
    }


    public void setLogin(Login Login) {
        this.Login = Login;
    }

    public Login getLogin() {
        return Login;
    }

    public void setId_deconsulta(String Id_deconsulta) {
        this.Id_deconsulta = Id_deconsulta;
    }

    public String getId_deconsulta() {
        return Id_deconsulta;
    }

    public void setTipo_comentarios(String Tipo_comentarios) {
        this.Tipo_comentarios = Tipo_comentarios;
    }

    public String getTipo_comentarios() {
        return Tipo_comentarios;
    }
}
