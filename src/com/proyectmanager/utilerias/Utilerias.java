package com.proyectmanager.utilerias;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import com.proyectmanager.objects.Constantes;
import com.proyectmanager.objects.Variables;

import com.sforce.soap.partner.sobject.SObject;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class Utilerias {
    public Utilerias() {
        super();
    }
    
    public HostnameVerifier disableSSL() throws NoSuchAlgorithmException, KeyManagementException {
        
        TrustManager[] trustAllCerts = new TrustManager[] {
          new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
              return null;
            }
            public void checkClientTrusted(X509Certificate[] certs, String authType) {}
            public void checkServerTrusted(X509Certificate[] certs, String authType) {}
          }
        };
        
        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        
        HostnameVerifier allHostsValid = new HostnameVerifier() {
          public boolean verify(String hostname, SSLSession session) {
            return true;
          }
        };
        
        return allHostsValid;
            
    }
    
    public Object strJsonToClass(String jsonInput, Class<?> claseConversion){
        
        try{
        
            Gson gson_object = new Gson();
            JsonParser parser = new JsonParser();
            JsonElement element = parser.parse(jsonInput);
            
            return gson_object.fromJson(element, claseConversion);
        
        }catch(Exception ex){
            System.err.println("error : " + ex.toString());
            return null;
        }
                
    }
    
    public String classTostrJson(Object classToJson){
        try{
            Gson gson = new Gson();
            
            return gson.toJson(classToJson);
        }catch(Exception ex){
            System.out.println("Error json to class : " + ex);
            return "{}";
        }    
    }
    
    public Properties getConfiguracion(){
        Properties prop = null;
        try{
            prop = new Properties();
            prop.load(this.getClass().getResourceAsStream(Constantes.PATH_PROPERTIES));
            
            return prop;
        }catch(Exception ex){
            System.err.println("Error al obtener propiedades = " + ex);
            return prop;
        }
    }
    
    public Properties getConfiguracionBD(){
        
        Properties prop = new Properties();
        
        try{
            
             Map<String,String> configuraciones = Constantes.CRUD_FFM.configuracionGeneral();
              
              if(!configuraciones.isEmpty()){
                  for ( Map.Entry<String, String> entry : configuraciones.entrySet()) {
                      String key = entry.getKey();
                      String value = entry.getValue();
                      prop.put(key, value);  
                  }
              }
            
        }catch(Exception ex){
            System.out.println("Error propiedades BD " + ex.toString());
        }
        
        return prop;
    }
    
    
    public void loadConfigIN_DB(){
        
        try{
            
            Properties prop_in_db = this.getConfiguracionBD();
            Variables.setSF_USER(prop_in_db.getProperty(Constantes.KEY_USER_SF));
            Variables.setSF_PASSWORD(prop_in_db.getProperty(Constantes.KEY_PASSWORD_SF));
            Variables.setSF_END_POINT(prop_in_db.getProperty(Constantes.KEY_END_POINT_SF));
            Variables.setQUERY_CONSULTA_COTIZACION(prop_in_db.getProperty(Constantes.KEY_GET_COTS_SF));
            Variables.setQUERY_CONSULTA_PUNTAS(prop_in_db.getProperty(Constantes.KEY_GET_PUNTAS_SF));
            Variables.setQUERY_CONSULTA_CATALOGOS(prop_in_db.getProperty(Constantes.KEY_GET_CATS_SF));  
            Variables.setSEGUNDOS_DOS_DIAS(prop_in_db.getProperty(Constantes.KEY_SEGUNDOS_DOS_DIAS));
            Variables.setSEGUNDOS_MEDIODIA(prop_in_db.getProperty(Constantes.KEY_SEGUNDOS_MEDIODIA));
            Variables.setSEGUNDOS_UNDIA(prop_in_db.getProperty(Constantes.KEY_SEGUNDOS_UNDIA));
            Variables.setSEGUNDOS_LESS(prop_in_db.getProperty(Constantes.KEY_SEGUNDOS_LESS));
            Variables.setSEGUNDOS_MORELESS(prop_in_db.getProperty(Constantes.KEY_SEGUNDOS_MORELESS));
            Variables.setSEGUNDOS_OK(prop_in_db.getProperty(Constantes.KEY_SEGUNDOS_OK));
            Variables.setSEGUNDOS_OK_PLANING(prop_in_db.getProperty(Constantes.KEY_SEGUNDOS_OK_PLANING));
            Variables.setSEGUNDOS_RIESGO_PLANING(prop_in_db.getProperty(Constantes.KEY_SEGUNDOS_RIESGO_PLANING));
            Variables.setSEGUNDOS_FUERA_PLANING(prop_in_db.getProperty(Constantes.KEY_SEGUNDOS_FUERA_PLANING));
            Variables.setPORCENTAJE_OK(prop_in_db.getProperty(Constantes.KEY_PORCENTAJE_OK));
            Variables.setPORCENTAJE_RIESGO(prop_in_db.getProperty(Constantes.KEY_PORCENTAJE_RIESGO));
            Variables.setPORCENTAJE_FUERA(prop_in_db.getProperty(Constantes.KEY_PORCENTAJE_FUERA)); 
            Variables.setKPI_PE(prop_in_db.getProperty(Constantes.KEY_KPI_PE_DIAS));
                        
        }catch(Exception ex){
            System.err.println("Error al cargar configuracion en DB : " + ex.toString());
        }    
    }    
    
    public Calendar getDateSF(String fechaAgenda_sf){
        
        try{

            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Date date = sdf.parse(fechaAgenda_sf);
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);            
            return cal; 
            
        }catch(Exception ex){
            System.out.println("error fecha sf : " + ex.toString());
            return null;
        }
        
    }
    
    public String tiempoTranscurrido(String fecha_creacion){
        
        try{
            
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
     
            Date fechaFinal=new Date();
            Date fechaInicial=format.parse(fecha_creacion);
    
            int diferencia=(int) ((fechaFinal.getTime()-fechaInicial.getTime())/1000);
            int dias=0;
            int horas=0;
            int minutos=0;
            
            if(diferencia>86400) {
                dias=(int)Math.floor(diferencia/86400);
                diferencia=diferencia-(dias*86400);
            }
            if(diferencia>3600) {
                horas=(int)Math.floor(diferencia/3600);
                diferencia=diferencia-(horas*3600);
            }
            if(diferencia>60) {
                minutos=(int)Math.floor(diferencia/60);
                diferencia=diferencia-(minutos*60);
            }
            
            return dias+" dias, "+horas+" horas, "+minutos+" minutos";
           
        }catch(Exception ex){
            System.out.println("Error : " + ex.toString());
            return "No se pudo estimar el tiempo de demora";
        }
    }
    
    public Boolean validateDataSF(SObject contenedor, String campoValidar){
        
        try{
            if(contenedor.getSObjectField(campoValidar) != null && contenedor.getSObjectField(campoValidar).getClass().equals(SObject.class)){
                 SObject objeto_sf = (SObject) contenedor.getSObjectField(campoValidar); 
                 if (objeto_sf != null) {
                     return true;
                 }else{
                    return false;    
                 }
            }else{
                return false;    
            }
        }catch(Exception ex){
            return false;    
        }
    }
}
