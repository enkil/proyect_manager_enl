package com.proyectmanager.objects;


public class Proyecto {
    
    private String Nombre_cliente;
    private String Id_cotizacion;
    private String Folio_cotizacion;
    //-- datos en planeacion 
    private String Fecha_inicio; 
    private String Fecha_fin;
    private String Fecha_fin_Planeada;
    private String Avance;
    //-- En mesa de control
    private String Nombre_Contacto;
    private String Telefono_contacto;
    private String Fecha_creacion;
    //-- Datos en pendientes planeacion
    private String Segmento;
    private String Numero_sitios;
    private String Tiempo_mesa;
    private String Semaforo;
    private String PorcentajeEsperado;
    
    public Proyecto() {
        super();
    }

    public void setNombre_cliente(String Nombre_cliente) {
        this.Nombre_cliente = Nombre_cliente;
    }

    public String getNombre_cliente() {
        return Nombre_cliente;
    }

    public void setId_cotizacion(String Id_cotizacion) {
        this.Id_cotizacion = Id_cotizacion;
    }

    public String getId_cotizacion() {
        return Id_cotizacion;
    }

    public void setFolio_cotizacion(String Folio_cotizacion) {
        this.Folio_cotizacion = Folio_cotizacion;
    }

    public String getFolio_cotizacion() {
        return Folio_cotizacion;
    }

    public void setFecha_inicio(String Fecha_inicio) {
        this.Fecha_inicio = Fecha_inicio;
    }

    public String getFecha_inicio() {
        return Fecha_inicio;
    }

    public void setFecha_fin(String Fecha_fin) {
        this.Fecha_fin = Fecha_fin;
    }

    public String getFecha_fin() {
        return Fecha_fin;
    }

    public void setAvance(String Avance) {
        this.Avance = Avance;
    }

    public String getAvance() {
        return Avance;
    }

    public void setNombre_Contacto(String Nombre_Contacto) {
        this.Nombre_Contacto = Nombre_Contacto;
    }

    public String getNombre_Contacto() {
        return Nombre_Contacto;
    }

    public void setTelefono_contacto(String Telefono_contacto) {
        this.Telefono_contacto = Telefono_contacto;
    }

    public String getTelefono_contacto() {
        return Telefono_contacto;
    }

    public void setFecha_creacion(String Fecha_creacion) {
        this.Fecha_creacion = Fecha_creacion;
    }

    public String getFecha_creacion() {
        return Fecha_creacion;
    }

    public void setSegmento(String Segmento) {
        this.Segmento = Segmento;
    }

    public String getSegmento() {
        return Segmento;
    }

    public void setNumero_sitios(String Numero_sitios) {
        this.Numero_sitios = Numero_sitios;
    }

    public String getNumero_sitios() {
        return Numero_sitios;
    }

    public void setTiempo_mesa(String Tiempo_mesa) {
        this.Tiempo_mesa = Tiempo_mesa;
    }

    public String getTiempo_mesa() {
        return Tiempo_mesa;
    }

    public void setSemaforo(String Semaforo) {
        this.Semaforo = Semaforo;
    }

    public String getSemaforo() {
        return Semaforo;
    }

    public void setFecha_fin_Planeada(String Fecha_fin_Planeada) {
        this.Fecha_fin_Planeada = Fecha_fin_Planeada;
    }

    public String getFecha_fin_Planeada() {
        return Fecha_fin_Planeada;
    }

    public void setPorcentajeEsperado(String PorcentajeEsperado) {
        this.PorcentajeEsperado = PorcentajeEsperado;
    }

    public String getPorcentajeEsperado() {
        return PorcentajeEsperado;
    }
}
