package com.proyectmanager.objects;

public class Archivos {
    
    private String Id_archivo;
    private String Descripcion;
    private String Icono;
    private String Id_archivo_sf;
    private String Nombre_archivo;
    private String Tipo_archivo;
    private String Nombre_creaArchivo;
    private String Fecha_subida;
    private String Extencion_archivo;
    private String Version_archivo;        
    private String Body_archivo;
    
    public Archivos() {
        super();
    }

    public void setId_archivo_sf(String Id_archivo_sf) {
        this.Id_archivo_sf = Id_archivo_sf;
    }

    public String getId_archivo_sf() {
        return Id_archivo_sf;
    }

    public void setNombre_archivo(String Nombre_archivo) {
        this.Nombre_archivo = Nombre_archivo;
    }

    public String getNombre_archivo() {
        return Nombre_archivo;
    }

    public void setTipo_archivo(String Tipo_archivo) {
        this.Tipo_archivo = Tipo_archivo;
    }

    public String getTipo_archivo() {
        return Tipo_archivo;
    }

    public void setVersion_archivo(String Version_archivo) {
        this.Version_archivo = Version_archivo;
    }

    public String getVersion_archivo() {
        return Version_archivo;
    }

    public void setBody_archivo(String Body_archivo) {
        this.Body_archivo = Body_archivo;
    }

    public String getBody_archivo() {
        return Body_archivo;
    }

    public void setExtencion_archivo(String Extencion_archivo) {
        this.Extencion_archivo = Extencion_archivo;
    }

    public String getExtencion_archivo() {
        return Extencion_archivo;
    }

    public void setNombre_creaArchivo(String Nombre_creaArchivo) {
        this.Nombre_creaArchivo = Nombre_creaArchivo;
    }

    public String getNombre_creaArchivo() {
        return Nombre_creaArchivo;
    }

    public void setFecha_subida(String Fecha_subida) {
        this.Fecha_subida = Fecha_subida;
    }

    public String getFecha_subida() {
        return Fecha_subida;
    }

    public void setId_archivo(String Id_archivo) {
        this.Id_archivo = Id_archivo;
    }

    public String getId_archivo() {
        return Id_archivo;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setIcono(String Icono) {
        this.Icono = Icono;
    }

    public String getIcono() {
        return Icono;
    }

}
