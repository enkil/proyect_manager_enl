package com.proyectmanager.objects;


public class DetalleAgendamientoPM {
    
    private String Fecha;
    private String Turno;
    private String Id_ot;
    private String Orden_Servicio;
    
    public DetalleAgendamientoPM() {
        super();
    }

    public void setFecha(String Fecha) {
        this.Fecha = Fecha;
    }

    public String getFecha() {
        return Fecha;
    }

    public void setTurno(String Turno) {
        this.Turno = Turno;
    }

    public String getTurno() {
        return Turno;
    }

    public void setOrden_Servicio(String Orden_Servicio) {
        this.Orden_Servicio = Orden_Servicio;
    }

    public String getOrden_Servicio() {
        return Orden_Servicio;
    }

    public void setId_ot(String Id_ot) {
        this.Id_ot = Id_ot;
    }

    public String getId_ot() {
        return Id_ot;
    }
}
