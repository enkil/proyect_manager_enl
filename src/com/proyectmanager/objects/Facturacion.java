package com.proyectmanager.objects;

public class Facturacion {
    
    private String Costo;
    private String Capex;
    private String Presupuesto_autorizado;
    private String Instalacion_servicios;
    private String Opex_rentaMensual;
    private String Total_aprobado;
    private String Inversion_equipamiento;
    private String Presupuesto_rescate;
    private String Plazo;
    private String Facturacion_mensualUSD;
    private String Facturacion_mensualMXN;
    private String Tipo_CambioUSD_MX;
    private String Dif_facturacionMXN;
        
    public Facturacion() {
        super();
    }

    public void setCosto(String Costo) {
        this.Costo = Costo;
    }

    public String getCosto() {
        return Costo;
    }

    public void setCapex(String Capex) {
        this.Capex = Capex;
    }

    public String getCapex() {
        return Capex;
    }

    public void setPresupuesto_autorizado(String Presupuesto_autorizado) {
        this.Presupuesto_autorizado = Presupuesto_autorizado;
    }

    public String getPresupuesto_autorizado() {
        return Presupuesto_autorizado;
    }

    public void setInstalacion_servicios(String Instalacion_servicios) {
        this.Instalacion_servicios = Instalacion_servicios;
    }

    public String getInstalacion_servicios() {
        return Instalacion_servicios;
    }

    public void setOpex_rentaMensual(String Opex_rentaMensual) {
        this.Opex_rentaMensual = Opex_rentaMensual;
    }

    public String getOpex_rentaMensual() {
        return Opex_rentaMensual;
    }

    public void setTotal_aprobado(String Total_aprobado) {
        this.Total_aprobado = Total_aprobado;
    }

    public String getTotal_aprobado() {
        return Total_aprobado;
    }

    public void setInversion_equipamiento(String Inversion_equipamiento) {
        this.Inversion_equipamiento = Inversion_equipamiento;
    }

    public String getInversion_equipamiento() {
        return Inversion_equipamiento;
    }

    public void setPresupuesto_rescate(String Presupuesto_rescate) {
        this.Presupuesto_rescate = Presupuesto_rescate;
    }

    public String getPresupuesto_rescate() {
        return Presupuesto_rescate;
    }

    public void setPlazo(String Plazo) {
        this.Plazo = Plazo;
    }

    public String getPlazo() {
        return Plazo;
    }

    public void setFacturacion_mensualUSD(String Facturacion_mensualUSD) {
        this.Facturacion_mensualUSD = Facturacion_mensualUSD;
    }

    public String getFacturacion_mensualUSD() {
        return Facturacion_mensualUSD;
    }

    public void setFacturacion_mensualMXN(String Facturacion_mensualMXN) {
        this.Facturacion_mensualMXN = Facturacion_mensualMXN;
    }

    public String getFacturacion_mensualMXN() {
        return Facturacion_mensualMXN;
    }

    public void setTipo_CambioUSD_MX(String Tipo_CambioUSD_MX) {
        this.Tipo_CambioUSD_MX = Tipo_CambioUSD_MX;
    }

    public String getTipo_CambioUSD_MX() {
        return Tipo_CambioUSD_MX;
    }

    public void setDif_facturacionMXN(String Dif_facturacionMXN) {
        this.Dif_facturacionMXN = Dif_facturacionMXN;
    }

    public String getDif_facturacionMXN() {
        return Dif_facturacionMXN;
    }
}
