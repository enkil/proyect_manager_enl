package com.proyectmanager.objects;


public class Responsables {
    
    private String Id_Responsable;
    private String Nombre_completo;
    private String Nombre;
    private String ApellidoPaterno;
    private String ApellidoMaterno;
    private String Num_Telefonico;
    
    public Responsables() {
        super();
    }


    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setApellidoPaterno(String ApellidoPaterno) {
        this.ApellidoPaterno = ApellidoPaterno;
    }

    public String getApellidoPaterno() {
        return ApellidoPaterno;
    }

    public void setApellidoMaterno(String ApellidoMaterno) {
        this.ApellidoMaterno = ApellidoMaterno;
    }

    public String getApellidoMaterno() {
        return ApellidoMaterno;
    }

    public void setNum_Telefonico(String Num_Telefonico) {
        this.Num_Telefonico = Num_Telefonico;
    }

    public String getNum_Telefonico() {
        return Num_Telefonico;
    }

    public void setNombre_completo(String Nombre_completo) {
        this.Nombre_completo = Nombre_completo;
    }

    public String getNombre_completo() {
        return Nombre_completo;
    }


    public void setId_Responsable(String Id_Responsable) {
        this.Id_Responsable = Id_Responsable;
    }

    public String getId_Responsable() {
        return Id_Responsable;
    }

}
