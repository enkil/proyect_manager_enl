package com.proyectmanager.objects;


public class Variables {
    
    private static String SF_USER;
    private static String SF_PASSWORD;
    private static String SF_END_POINT;
    private static String QUERY_CONSULTA_COTIZACION;
    private static String QUERY_CONSULTA_PUNTAS;
    private static String QUERY_CONSULTA_CATALOGOS;
    //private static String QR_BANDEJA_MESA_CONTROL;
    //private static String QR_BANDEJA_EN_PLANEACION;
    //private static String QR_BANDEJA_POR_PLANEAR;
    //private static String QR_BANDEJA_FACTURANDO;
    //private static String COLOR_FUERA_TIEMPO;
    //private static String COLOR_RIESGO;
    //private static String COLOR_ENTIEMPO;
    private static String SEGUNDOS_DOS_DIAS;
    private static String SEGUNDOS_MEDIODIA;
    private static String SEGUNDOS_UNDIA;
    private static String SEGUNDOS_LESS;
    private static String SEGUNDOS_MORELESS;
    private static String SEGUNDOS_OK;
    private static String SEGUNDOS_OK_PLANING;
    private static String SEGUNDOS_RIESGO_PLANING;
    private static String SEGUNDOS_FUERA_PLANING;
    private static String PORCENTAJE_OK;
    private static String PORCENTAJE_RIESGO;
    private static String PORCENTAJE_FUERA;  
    private static String KPI_PE;
    
    
    public Variables() {
        super();
    }


    public static void setSF_USER(String SF_USER) {
        Variables.SF_USER = SF_USER;
    }

    public static String getSF_USER() {
        return SF_USER;
    }

    public static void setSF_PASSWORD(String SF_PASSWORD) {
        Variables.SF_PASSWORD = SF_PASSWORD;
    }

    public static String getSF_PASSWORD() {
        return SF_PASSWORD;
    }

    public static void setSF_END_POINT(String SF_END_POINT) {
        Variables.SF_END_POINT = SF_END_POINT;
    }

    public static String getSF_END_POINT() {
        return SF_END_POINT;
    }

    public static void setQUERY_CONSULTA_COTIZACION(String QUERY_CONSULTA_COTIZACION) {
        Variables.QUERY_CONSULTA_COTIZACION = QUERY_CONSULTA_COTIZACION;
    }

    public static String getQUERY_CONSULTA_COTIZACION() {
        return QUERY_CONSULTA_COTIZACION;
    }

    public static void setQUERY_CONSULTA_PUNTAS(String QUERY_CONSULTA_PUNTAS) {
        Variables.QUERY_CONSULTA_PUNTAS = QUERY_CONSULTA_PUNTAS;
    }

    public static String getQUERY_CONSULTA_PUNTAS() {
        return QUERY_CONSULTA_PUNTAS;
    }

    public static void setQUERY_CONSULTA_CATALOGOS(String QUERY_CONSULTA_CATALOGOS) {
        Variables.QUERY_CONSULTA_CATALOGOS = QUERY_CONSULTA_CATALOGOS;
    }

    public static String getQUERY_CONSULTA_CATALOGOS() {
        return QUERY_CONSULTA_CATALOGOS;
    }

   /* public static void setQR_BANDEJA_MESA_CONTROL(String QR_BANDEJA_MESA_CONTROL) {
        Variables.QR_BANDEJA_MESA_CONTROL = QR_BANDEJA_MESA_CONTROL;
    }

    public static String getQR_BANDEJA_MESA_CONTROL() {
        return QR_BANDEJA_MESA_CONTROL;
    }

    public static void setQR_BANDEJA_EN_PLANEACION(String QR_BANDEJA_EN_PLANEACION) {
        Variables.QR_BANDEJA_EN_PLANEACION = QR_BANDEJA_EN_PLANEACION;
    }

    public static String getQR_BANDEJA_EN_PLANEACION() {
        return QR_BANDEJA_EN_PLANEACION;
    }

    public static void setQR_BANDEJA_POR_PLANEAR(String QR_BANDEJA_POR_PLANEAR) {
        Variables.QR_BANDEJA_POR_PLANEAR = QR_BANDEJA_POR_PLANEAR;
    }

    public static String getQR_BANDEJA_POR_PLANEAR() {
        return QR_BANDEJA_POR_PLANEAR;
    }

    public static void setQR_BANDEJA_FACTURANDO(String QR_BANDEJA_FACTURANDO) {
        Variables.QR_BANDEJA_FACTURANDO = QR_BANDEJA_FACTURANDO;
    }

    public static String getQR_BANDEJA_FACTURANDO() {
        return QR_BANDEJA_FACTURANDO;
    }*/

    /*public static void setCOLOR_FUERA_TIEMPO(String COLOR_FUERA_TIEMPO) {
        Variables.COLOR_FUERA_TIEMPO = COLOR_FUERA_TIEMPO;
    }

    public static String getCOLOR_FUERA_TIEMPO() {
        return COLOR_FUERA_TIEMPO;
    }

    public static void setCOLOR_RIESGO(String COLOR_RIESGO) {
        Variables.COLOR_RIESGO = COLOR_RIESGO;
    }

    public static String getCOLOR_RIESGO() {
        return COLOR_RIESGO;
    }

    public static void setCOLOR_ENTIEMPO(String COLOR_ENTIEMPO) {
        Variables.COLOR_ENTIEMPO = COLOR_ENTIEMPO;
    }

    public static String getCOLOR_ENTIEMPO() {
        return COLOR_ENTIEMPO;
    }*/

    public static void setSEGUNDOS_DOS_DIAS(String SEGUNDOS_DOS_DIAS) {
        Variables.SEGUNDOS_DOS_DIAS = SEGUNDOS_DOS_DIAS;
    }

    public static String getSEGUNDOS_DOS_DIAS() {
        return SEGUNDOS_DOS_DIAS;
    }

    public static void setSEGUNDOS_MEDIODIA(String SEGUNDOS_MEDIODIA) {
        Variables.SEGUNDOS_MEDIODIA = SEGUNDOS_MEDIODIA;
    }

    public static String getSEGUNDOS_MEDIODIA() {
        return SEGUNDOS_MEDIODIA;
    }

    public static void setSEGUNDOS_UNDIA(String SEGUNDOS_UNDIA) {
        Variables.SEGUNDOS_UNDIA = SEGUNDOS_UNDIA;
    }

    public static String getSEGUNDOS_UNDIA() {
        return SEGUNDOS_UNDIA;
    }

    public static void setSEGUNDOS_LESS(String SEGUNDOS_LESS) {
        Variables.SEGUNDOS_LESS = SEGUNDOS_LESS;
    }

    public static String getSEGUNDOS_LESS() {
        return SEGUNDOS_LESS;
    }

    public static void setSEGUNDOS_MORELESS(String SEGUNDOS_MORELESS) {
        Variables.SEGUNDOS_MORELESS = SEGUNDOS_MORELESS;
    }

    public static String getSEGUNDOS_MORELESS() {
        return SEGUNDOS_MORELESS;
    }

    public static void setSEGUNDOS_OK(String SEGUNDOS_OK) {
        Variables.SEGUNDOS_OK = SEGUNDOS_OK;
    }

    public static String getSEGUNDOS_OK() {
        return SEGUNDOS_OK;
    }

    public static void setSEGUNDOS_OK_PLANING(String SEGUNDOS_OK_PLANING) {
        Variables.SEGUNDOS_OK_PLANING = SEGUNDOS_OK_PLANING;
    }

    public static String getSEGUNDOS_OK_PLANING() {
        return SEGUNDOS_OK_PLANING;
    }

    public static void setSEGUNDOS_RIESGO_PLANING(String SEGUNDOS_RIESGO_PLANING) {
        Variables.SEGUNDOS_RIESGO_PLANING = SEGUNDOS_RIESGO_PLANING;
    }

    public static String getSEGUNDOS_RIESGO_PLANING() {
        return SEGUNDOS_RIESGO_PLANING;
    }

    public static void setSEGUNDOS_FUERA_PLANING(String SEGUNDOS_FUERA_PLANING) {
        Variables.SEGUNDOS_FUERA_PLANING = SEGUNDOS_FUERA_PLANING;
    }

    public static String getSEGUNDOS_FUERA_PLANING() {
        return SEGUNDOS_FUERA_PLANING;
    }

    public static void setPORCENTAJE_OK(String PORCENTAJE_OK) {
        Variables.PORCENTAJE_OK = PORCENTAJE_OK;
    }

    public static String getPORCENTAJE_OK() {
        return PORCENTAJE_OK;
    }

    public static void setPORCENTAJE_RIESGO(String PORCENTAJE_RIESGO) {
        Variables.PORCENTAJE_RIESGO = PORCENTAJE_RIESGO;
    }

    public static String getPORCENTAJE_RIESGO() {
        return PORCENTAJE_RIESGO;
    }

    public static void setPORCENTAJE_FUERA(String PORCENTAJE_FUERA) {
        Variables.PORCENTAJE_FUERA = PORCENTAJE_FUERA;
    }

    public static String getPORCENTAJE_FUERA() {
        return PORCENTAJE_FUERA;
    }

    public static void setKPI_PE(String KPI_PE) {
        Variables.KPI_PE = KPI_PE;
    }

    public static String getKPI_PE() {
        return KPI_PE;
    }
}
