package com.proyectmanager.objects;

public class Comentarios {
    
    private String Id_comentario;
    private String Comentario;
    private String Fecha;
    private String Usuario_comenta;
    
    public Comentarios() {
        super();
    }

    public void setId_comentario(String Id_comentario) {
        this.Id_comentario = Id_comentario;
    }

    public String getId_comentario() {
        return Id_comentario;
    }

    public void setComentario(String Comentario) {
        this.Comentario = Comentario;
    }

    public String getComentario() {
        return Comentario;
    }

    public void setFecha(String Fecha) {
        this.Fecha = Fecha;
    }

    public String getFecha() {
        return Fecha;
    }

    public void setUsuario_comenta(String Usuario_comenta) {
        this.Usuario_comenta = Usuario_comenta;
    }

    public String getUsuario_comenta() {
        return Usuario_comenta;
    }
}
