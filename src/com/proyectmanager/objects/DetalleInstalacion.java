package com.proyectmanager.objects;


public class DetalleInstalacion {
    
    private String Nombre_Tecnico;
    private String Automovil;
    private String Estatus_instalacion;
    private String Estado_instalacion;
    private String Motivo_instalacion;
    
    public DetalleInstalacion() {
        super();
    }

    public void setNombre_Tecnico(String Nombre_Tecnico) {
        this.Nombre_Tecnico = Nombre_Tecnico;
    }

    public String getNombre_Tecnico() {
        return Nombre_Tecnico;
    }

    public void setAutomovil(String Automovil) {
        this.Automovil = Automovil;
    }

    public String getAutomovil() {
        return Automovil;
    }

    public void setEstatus_instalacion(String Estatus_instalacion) {
        this.Estatus_instalacion = Estatus_instalacion;
    }

    public String getEstatus_instalacion() {
        return Estatus_instalacion;
    }

    public void setEstado_instalacion(String Estado_instalacion) {
        this.Estado_instalacion = Estado_instalacion;
    }

    public String getEstado_instalacion() {
        return Estado_instalacion;
    }

    public void setMotivo_instalacion(String Motivo_instalacion) {
        this.Motivo_instalacion = Motivo_instalacion;
    }

    public String getMotivo_instalacion() {
        return Motivo_instalacion;
    }
}
