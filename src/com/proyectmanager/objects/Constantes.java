package com.proyectmanager.objects;

import com.proyectmanager.utilerias.ConexionDB;
import com.proyectmanager.utilerias.CrudFFM;
import com.proyectmanager.utilerias.CrudSF;
import com.proyectmanager.utilerias.Utilerias;

import java.util.Properties;


public class Constantes {
    
    //----- Instancias de clases
    public static final Utilerias UTILS = new Utilerias();
    public static final ConexionDB CONECTION_DB = new ConexionDB();
    public static final CrudSF CRUD_SF = new CrudSF();
    public static final CrudFFM CRUD_FFM = new CrudFFM();
    //----- contantes ws
    public static final String PATH_PROPERTIES = "/Resources/Configuracion.properties";
    public static final String COMENTARIOS_ACTIVIDAD = "A";
    public static final String COMENTARIOS_PUNTA = "P";
    public static final String TIPO_ARCHIVOS_ACTIVIDAD = "A";
    public static final String TIPO_ARCHIVOS_PUNTA = "P";
    public static final Properties CONFIG_PROPERTIES = UTILS.getConfiguracion();
    
    //----- Constantes desde Properties file    
    public static final String RESULT_SUSSES = CONFIG_PROPERTIES.getProperty("ws.susses_id");
    public static final String RESULT_ERROR = CONFIG_PROPERTIES.getProperty("ws.error_id");
    public static final String RESULTSESCRIPTION_SUSSES = CONFIG_PROPERTIES.getProperty("ws.msj_susses");
    public static final String WS_VERSION = CONFIG_PROPERTIES.getProperty("ws.version");
    
    //----- Constantes de conexion a bd
    public static final String DB_TYPE_CONECTION = CONFIG_PROPERTIES.getProperty("servidor.type_conection");
    public static final String DB_HOST = CONFIG_PROPERTIES.getProperty("servidor.host");
    public static final String DB_PORT = CONFIG_PROPERTIES.getProperty("servidor.port");
    public static final String DB_DBNAME = CONFIG_PROPERTIES.getProperty("servidor.namedb");
    public static final String DB_USER = CONFIG_PROPERTIES.getProperty("servidor.user");
    public static final String DB_PASSWORD = CONFIG_PROPERTIES.getProperty("servidor.password");
    public static final String DB_JNDI = CONFIG_PROPERTIES.getProperty("servidor.jndi");
    public static final String GENERIC_CONSULT = "Select Name From Account LIMIT 1";
    
    //-- Complementos 
    
    //-- Datos de configuracion conexion sf y configuracion en base 
    public static final String QR_GET_ID_SESSION_SF = CONFIG_PROPERTIES.getProperty("query.get_id_session_sf");
    public static final String QR_CONFIG_IN_DB = CONFIG_PROPERTIES.getProperty("query.conf_in_db");
    public static final String QR_UPDATE_SI_SF = CONFIG_PROPERTIES.getProperty("query.update_sessionid_sf");
    
    //-- Querys para catalogos
    public static final String QR_GET_RESPONSABLES =CONFIG_PROPERTIES.getProperty("query.get_responsable_db");
    public static final String QR_GET_ACTIVIDADES_TERCER_NIVEL = CONFIG_PROPERTIES.getProperty("query.get_actividades_tercer_nivel");
    public static final String QR_GET_LISTADOCUMENTOS = CONFIG_PROPERTIES.getProperty("query.get_lista_archivos");
    
    //-- Querys para insertar comentarios 
    public static final String QR_INSERTA_COMENTARIOS = CONFIG_PROPERTIES.getProperty("query.inserta_comentarios");
    
    //-- Query consulta comentarios 
    public static final String QR_GET_COMENTARIOS_ACTIVIDAD = CONFIG_PROPERTIES.getProperty("query.get_comentarios_actividad");
    public static final String QR_GET_COMENTARIOS_PUNTA = CONFIG_PROPERTIES.getProperty("query.get_comentarios_punta");
    
    //-- Query para consultar lista de archivos 
    public static final String QR_LISTA_ARCHIVOS_SF = CONFIG_PROPERTIES.getProperty("query.get_lista_archivos_sf");
    public static final String QR_CONSULTA_ARCHIVO_ACT_DB = CONFIG_PROPERTIES.getProperty("query.get_archivos_pm_db_actividad");
    public static final String QR_CONSULTA_ARCHIVO_PUNTA_DB = CONFIG_PROPERTIES.getProperty("query.get_Archivos_pm_db_punta");
    
    //-- Query para consultar archivos guardados 
    public static final String QR_CONSULTA_ARCHIVO_SF = CONFIG_PROPERTIES.getProperty("query.get_archivos_pm");
    
    //-- Querys para carga de archivos 
    public static final String QR_GET_CONTENT_DOCUMENT = CONFIG_PROPERTIES.getProperty("query_get_content_document");
    public static final String QR_SET_FILE_DB = CONFIG_PROPERTIES.getProperty("query.set_file_pm_db");
    
    
    //-- Querys para insertar nuevo responsable
    public static final String QR_GET_ID_RESPONSABLE = CONFIG_PROPERTIES.getProperty("query.get_secuencia_responsable");
    public static final String QR_SET_NEW_RESPONSABLE = CONFIG_PROPERTIES.getProperty("query.set_new_responsable");
    
    //---- Keys Properties DataBase
    public static final String KEY_USER_SF = "AUTENTICATION_SF_US";
    public static final String KEY_PASSWORD_SF = "AUTENTICATION_SF_PS";
    public static final String KEY_END_POINT_SF = "END_POINT_WS_SF";
    public static final String KEY_GET_COTS_SF = "QR_GET_COTIZACIONES_SF";
    public static final String KEY_GET_PUNTAS_SF = "QR_GET_PUNTAS_SF";
    public static final String KEY_GET_CATS_SF = "QR_GET_CAT_SF";
    public static final String KEY_SEGUNDOS_DOS_DIAS = "SEG_TWO_DAY";
    public static final String KEY_SEGUNDOS_MEDIODIA = "SEG_DAY_M";
    public static final String KEY_SEGUNDOS_UNDIA = "SEG_DAY";
    public static final String KEY_SEGUNDOS_LESS = "SEG_LESS";
    public static final String KEY_SEGUNDOS_MORELESS = "SEG_MORLESS";
    public static final String KEY_SEGUNDOS_OK = "SEG_OK";
    public static final String KEY_SEGUNDOS_OK_PLANING = "SEG_OK_PLANING";
    public static final String KEY_SEGUNDOS_RIESGO_PLANING = "SEG_RIESGO_PLANING";
    public static final String KEY_SEGUNDOS_FUERA_PLANING = "SEG_FUERA_PLANING";
    public static final String KEY_PORCENTAJE_OK = "PORCENTAJE_OK";
    public static final String KEY_PORCENTAJE_RIESGO = "PORCENTAJE_RIESGO";
    public static final String KEY_PORCENTAJE_FUERA = "PORCENTAJE_FUERATIEMPO";
    public static final String KEY_KPI_PE_DIAS = "KPI_PE_DIAS";
    
    public static final String Q_UPDATE_COMENTARIO_GENERAL = CONFIG_PROPERTIES.getProperty("query.u_actualiza_comentariogeteral");        
    
    public Constantes() {
        super();        
    }
    
    
}
