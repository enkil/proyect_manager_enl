package com.proyectmanager.methods;

import com.proyectmanager.inputs.InputUploadFileSalesforce;
import com.proyectmanager.outputs.OutputUploadFileSalesforce;
import com.proyectmanager.objects.Constantes;

import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.SaveResult;
import com.sforce.soap.partner.sobject.SObject;

import org.apache.commons.codec.binary.Base64;

import javax.ws.rs.core.Response;

public class UploadFileSalesforce {
    public UploadFileSalesforce() {
        super();
        Constantes.UTILS.loadConfigIN_DB();
    }
    
    public Response uploadFileSalesforce(String InputUploadFileSalesforce){
        InputUploadFileSalesforce input_ws = (InputUploadFileSalesforce) Constantes.UTILS.strJsonToClass(InputUploadFileSalesforce, InputUploadFileSalesforce.class);
        OutputUploadFileSalesforce output_ws = new OutputUploadFileSalesforce();
        
        try{
            String RelatedRecordId = " ";
            if(input_ws.getFile_Base64() != null && !input_ws.getFile_Base64().equals("")) {
                if (input_ws.getFile_Base64()  != null) {
                        SObject ContentVersion = new SObject();
                        ContentVersion.setType("ContentVersion");
                        ContentVersion.setField("PathOnClient", input_ws.getNombre_archivo()+"."+input_ws.getExtencion_archivo());
                        ContentVersion.setField("VersionData", Base64.decodeBase64(input_ws.getFile_Base64()));
                        String title = (input_ws.getNombre_archivo());
                        
                        ContentVersion.setField("Title", title);
                        SaveResult[] resultData = Constantes.CRUD_SF.create_sf(new SObject[] { ContentVersion });
                        for (int i = 0; i < resultData.length; i++) {
                                if (resultData[i].isSuccess()) {
                                        RelatedRecordId = (String)resultData[i].getId();
                                        String ContentDocumentId = "";
                                                       
                                        QueryResult comentariosN1 = Constantes.CRUD_SF.query_sf(Constantes.QR_GET_CONTENT_DOCUMENT.replaceAll("RelatedRecordId", RelatedRecordId));
                                        if (comentariosN1.getDone()) {
                                                SObject[] qresultN1 = comentariosN1.getRecords();
                                                String id_file = "";
                                            if(qresultN1[0].getSObjectField("ContentDocument") != null && qresultN1[0].getSObjectField("ContentDocument").getClass().equals(SObject.class)){
                                                SObject doc = (SObject) qresultN1[0].getSObjectField("ContentDocument"); 
                                                    if (doc != null) {
                                                        if(doc.getSObjectField("LatestPublishedVersion") != null && doc.getSObjectField("LatestPublishedVersion").getClass().equals(SObject.class)){
                                                            SObject lespub = (SObject) doc.getSObjectField("LatestPublishedVersion"); 
                                                                if (lespub != null) {
                                                                    id_file = (String) lespub.getField("Id");
                                                                }
                                                        }
                                                    }
                                            }
                                        
                                                if (qresultN1.length > 0) {
                                                    if (qresultN1[0].getType().equals("ContentVersion")) {
                                                                for (int j = 0; j < qresultN1.length; j++) {
                                                                        ContentDocumentId = (String)qresultN1[j].getField("ContentDocumentId");
                                                                }
                                                                
                                                                SObject ContentDocumentLink  = new SObject();
                                                                ContentDocumentLink.setType("ContentDocumentLink");
                                                                ContentDocumentLink.setField("ContentDocumentId", ContentDocumentId);
                                                                ContentDocumentLink.setField("LinkedEntityId", input_ws.getId_objeto_sf());
                                                                ContentDocumentLink.setField("ShareType", "V");
                                                                SaveResult[] resultDataLink = Constantes.CRUD_SF.create_sf(new SObject[] { ContentDocumentLink });
                                                                for (int k = 0; k < resultDataLink.length; k++) {
                                                                        if (resultDataLink[k].isSuccess()) {
                                                                            //resultDataLink[i].getId()
                                                                            System.out.println("Consulta : " + Constantes.QR_SET_FILE_DB.replaceAll("tipo_archivo", input_ws.getId_tipo_imagen())
                                                                            .replaceAll("id_objeto_Sf", input_ws.getId_objeto_sf()).replaceAll("id_archivo_sf",id_file)
                                                                            .replaceAll("id_actividad", input_ws.getId_actividad().replaceAll("", "NULL"))
                                                                            .replaceAll("id_user_crea", input_ws.getId_user_uploadFile()));
                                                                            
                                                                            Boolean insert_file_db = Constantes.CRUD_FFM.insertaDB(Constantes.QR_SET_FILE_DB.replaceAll("tipo_archivo", input_ws.getId_tipo_imagen())
                                                                            .replaceAll("id_objeto_Sf", input_ws.getId_objeto_sf()).replaceAll("id_archivo_sf",id_file)
                                                                                                                                   .replaceAll("id_actividad", input_ws.getId_actividad().equals("") ? "NULL" : input_ws.getId_actividad())
                                                                            .replaceAll("id_user_crea", input_ws.getId_user_uploadFile()));
                                                                            if(insert_file_db){
                                                                                output_ws.setResult(Constantes.RESULT_SUSSES);
                                                                                output_ws.setResultDescripcion(Constantes.RESULTSESCRIPTION_SUSSES);
                                                                                output_ws.setVersion(Constantes.WS_VERSION);      
                                                                            }else{
                                                                                output_ws.setResult(Constantes.RESULT_ERROR);
                                                                                output_ws.setResultDescripcion("Error Al Guardar Archivo FFM");
                                                                                output_ws.setVersion(Constantes.WS_VERSION);
                                                                            }
                                                                              
                                                                        }else {
                                                                            output_ws.setResult(Constantes.RESULT_ERROR);
                                                                            output_ws.setResultDescripcion(resultDataLink[i].getErrors()[i].getMessage());
                                                                            output_ws.setVersion(Constantes.WS_VERSION);
                                                                        }
                                                                }
                                                                
                                                        }
                                                }else {
                                                    output_ws.setResult(Constantes.RESULT_ERROR);
                                                    output_ws.setResultDescripcion("Error Al Guardar Archivo");
                                                    output_ws.setVersion(Constantes.WS_VERSION);
                                                }
                                        }else {
                                            output_ws.setResult(Constantes.RESULT_ERROR);
                                            output_ws.setResultDescripcion("Error Al Guardar Archivo");
                                            output_ws.setVersion(Constantes.WS_VERSION);
                                        }
                                        
                                } else {
                                    output_ws.setResult(Constantes.RESULT_ERROR);
                                    output_ws.setResultDescripcion("Error Al Guardar Archivo : " + resultData[i].getErrors()[i].getMessage());
                                    output_ws.setVersion(Constantes.WS_VERSION);
                                        
                                }
                        }
                } else {
                    output_ws.setResult(Constantes.RESULT_ERROR);
                    output_ws.setResultDescripcion("Error en la convercion del archivo");
                    output_ws.setVersion(Constantes.WS_VERSION);
                }
            }
        }catch(Exception ex){
            output_ws.setResult(Constantes.RESULT_ERROR);
            output_ws.setResultDescripcion("ERROR : " + ex.toString() +" - " + Constantes.UTILS.classTostrJson(input_ws));
            output_ws.setVersion(Constantes.WS_VERSION);
        }catch(Throwable exs){
            output_ws.setResult(Constantes.RESULT_ERROR);
            output_ws.setResultDescripcion("ERROR T : " + exs.toString());
            output_ws.setVersion(Constantes.WS_VERSION);
        }
        
        System.out.println(Constantes.UTILS.classTostrJson(output_ws));
        
        return Response.status(200).entity(Constantes.UTILS.classTostrJson(output_ws)).build();
    }
    
}
