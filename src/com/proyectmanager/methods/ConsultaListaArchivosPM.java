package com.proyectmanager.methods;

import com.proyectmanager.inputs.InputConsultaListaArchivosPM;
import com.proyectmanager.outputs.OutputConsultaListaArchivosPM;
import com.proyectmanager.objects.Archivos;
import com.proyectmanager.objects.Constantes;

import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.sobject.SObject;

import javax.ws.rs.core.Response;

public class ConsultaListaArchivosPM {
    public ConsultaListaArchivosPM() {
        super();
        Constantes.UTILS.loadConfigIN_DB();
    }
    
    public Response consultaListaArchivosPM(String InputConsultaListaArchivosPM){
        InputConsultaListaArchivosPM input_ws = (InputConsultaListaArchivosPM) Constantes.UTILS.strJsonToClass(InputConsultaListaArchivosPM, InputConsultaListaArchivosPM.class);
        OutputConsultaListaArchivosPM output_ws = new OutputConsultaListaArchivosPM();
        
        try{
            String consulta = "";
            switch(input_ws.getTipo_archivos()){
                case Constantes.TIPO_ARCHIVOS_ACTIVIDAD:
                    consulta = Constantes.QR_CONSULTA_ARCHIVO_ACT_DB.replaceAll("id_actividad", input_ws.getId_consulta());
                break;
                case Constantes.TIPO_ARCHIVOS_PUNTA:
                    consulta = Constantes.QR_CONSULTA_ARCHIVO_PUNTA_DB.replaceAll("id_punta", input_ws.getId_consulta());
                break;
            }
            
            String[][] arr_archivos_db = Constantes.CRUD_FFM.consultaBD(consulta);
            
            if(arr_archivos_db.length > 0){
                QueryResult lista_archivos_sf = Constantes.CRUD_SF.query_sf(Constantes.QR_LISTA_ARCHIVOS_SF.replaceAll("id_objecto_sf",input_ws.getTipo_archivos().equals(Constantes.TIPO_ARCHIVOS_ACTIVIDAD) ? arr_archivos_db[0][4] : input_ws.getId_consulta()));
                Archivos[] arr_Archivos = new Archivos[arr_archivos_db.length];
                
                Integer tamano_arreglo = 0;
                for(SObject file_sf : lista_archivos_sf.getRecords()){
                    if(file_sf.getSObjectField("ContentDocument") != null && file_sf.getSObjectField("ContentDocument").getClass().equals(SObject.class)){
                        SObject cont_doc = (SObject) file_sf.getSObjectField("ContentDocument"); 
                            if (cont_doc != null) {
                                if(cont_doc.getSObjectField("LatestPublishedVersion") != null && cont_doc.getSObjectField("LatestPublishedVersion").getClass().equals(SObject.class)){
                                    SObject publics = (SObject) cont_doc.getSObjectField("LatestPublishedVersion"); 
                                        if (publics != null && !publics.getField("FileType").equals("UNKNOWN")){
                                            for(int row = 0; row < arr_archivos_db.length; row++){
                                                if(arr_archivos_db[row][1].equals((String) publics.getField("Id"))){
                                                    tamano_arreglo++;       
                                                }
                                            }
                                        }    
                                    }
                                }
                            }  
                }
                
                arr_Archivos = new Archivos[tamano_arreglo];
                int contador = 0;
                for(SObject file_sf : lista_archivos_sf.getRecords()){
                    if(file_sf.getSObjectField("ContentDocument") != null && file_sf.getSObjectField("ContentDocument").getClass().equals(SObject.class)){
                        SObject cont_doc = (SObject) file_sf.getSObjectField("ContentDocument"); 
                            if (cont_doc != null) {
                                if(cont_doc.getSObjectField("LatestPublishedVersion") != null && cont_doc.getSObjectField("LatestPublishedVersion").getClass().equals(SObject.class)){
                                    SObject publics = (SObject) cont_doc.getSObjectField("LatestPublishedVersion"); 
                                        if (publics != null && !publics.getField("FileType").equals("UNKNOWN")){
                                            for(int row = 0; row < arr_archivos_db.length; row++){
                                                if(arr_archivos_db[row][1].equals((String) publics.getField("Id"))){
                                                    Archivos arc = new Archivos();
                                                    arc.setId_archivo_sf((String) publics.getSObjectField("Id"));
                                                    arc.setNombre_archivo((String) publics.getField("Title"));
                                                    arc.setExtencion_archivo((String) publics.getField("FileExtension"));
                                                    arc.setTipo_archivo(arr_archivos_db[row][0]);
                                                    arc.setNombre_creaArchivo(arr_archivos_db[row][2]);
                                                    arc.setFecha_subida(arr_archivos_db[row][3]);
                                                    arr_Archivos[contador++] = arc;
                                                }
                                            }
                                        }    
                                    }
                                }
                            }  
                }
                
                output_ws.setResult(Constantes.RESULT_SUSSES);
                output_ws.setResultDescripcion(Constantes.RESULTSESCRIPTION_SUSSES);
                output_ws.setVersion(Constantes.WS_VERSION);
                output_ws.setArchivos(arr_Archivos);                
            }else{
                output_ws.setResult(Constantes.RESULT_SUSSES);
                output_ws.setResultDescripcion("No se encontraron archivos");
                output_ws.setVersion(Constantes.WS_VERSION);
            }
        
        }catch(Exception ex){
            output_ws.setResult(Constantes.RESULT_ERROR);
            output_ws.setResultDescripcion("ERROR : " + ex.toString());
            output_ws.setVersion(Constantes.WS_VERSION);
        }catch (Throwable exs) {
            output_ws.setResult(Constantes.RESULT_ERROR);
            output_ws.setResultDescripcion("ERROR : " + exs.toString());
            output_ws.setVersion(Constantes.WS_VERSION);
        }
        
        System.out.println(Constantes.UTILS.classTostrJson(output_ws));
        
        return Response.status(200).entity(Constantes.UTILS.classTostrJson(output_ws)).build();
    }
}
