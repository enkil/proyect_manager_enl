package com.proyectmanager.methods;

import com.proyectmanager.inputs.InputInsertaComentariosPM;
import com.proyectmanager.outputs.OutputInsertaComentariosPM;
import com.proyectmanager.objects.Constantes;

import javax.ws.rs.core.Response;

public class InsertaComentariosPM {
    public InsertaComentariosPM() {
        super();
        Constantes.UTILS.loadConfigIN_DB();
    }
    
    public Response insertaComentariosPM(String InputInsertaComentariosPM){
        InputInsertaComentariosPM input_ws = (InputInsertaComentariosPM) Constantes.UTILS.strJsonToClass(InputInsertaComentariosPM, InputInsertaComentariosPM.class);
        OutputInsertaComentariosPM output_ws = new OutputInsertaComentariosPM();
        
        try{
            
            Boolean inserta_comentarios = Constantes.CRUD_FFM.insertaDB(Constantes.QR_INSERTA_COMENTARIOS.replaceAll("comentarios", input_ws.getComentario()).replaceAll("id_usuario", input_ws.getId_usuario()).replaceAll("id_cs", input_ws.getId_cs()).replaceAll("id_actividad", input_ws.getId_actividad().equals("") ? "NULL" : input_ws.getId_actividad()));
            
            if(inserta_comentarios){
                
                if(input_ws.getId_cs()!= null && !input_ws.getId_cs().equals("")){
                    Constantes.CRUD_FFM.actualizaDB(Constantes.Q_UPDATE_COMENTARIO_GENERAL.replaceAll("comentario", input_ws.getComentario()).replaceAll("idBridgeSf", input_ws.getId_cs()));
                }
                output_ws.setResult(Constantes.RESULT_SUSSES);
                output_ws.setResultDescripcion(Constantes.RESULTSESCRIPTION_SUSSES);
                output_ws.setVersion(Constantes.WS_VERSION);
            }else{
                output_ws.setResult(Constantes.RESULT_ERROR);
                output_ws.setResultDescripcion("Error al insertar comentarios");
                output_ws.setVersion(Constantes.WS_VERSION);
            }
            
        }catch(Exception ex){
            output_ws.setResult(Constantes.RESULT_ERROR);
            output_ws.setResultDescripcion("ERROR : " + ex.toString());
            output_ws.setVersion(Constantes.WS_VERSION);
            System.out.println("Error : " + ex.toString());
        }catch (Throwable exs) {
            output_ws.setResult(Constantes.RESULT_ERROR);
            output_ws.setResultDescripcion("ERROR : " + exs.toString());
            output_ws.setVersion(Constantes.WS_VERSION);
        }
        
        System.out.println(Constantes.UTILS.classTostrJson(output_ws));
        
        return Response.status(200).entity(Constantes.UTILS.classTostrJson(output_ws)).build();        
    }
}
