package com.proyectmanager.methods;

import com.proyectmanager.inputs.InputConsultaComentariosPM;
import com.proyectmanager.outputs.OutputConsultaComentariosPM;
import com.proyectmanager.objects.Comentarios;
import com.proyectmanager.objects.Constantes;

import javax.ws.rs.core.Response;

public class ConsultaComentariosPM {
    public ConsultaComentariosPM() {
        super();
        Constantes.UTILS.loadConfigIN_DB();
    }
    
    public Response consultaComentariosPM(String InputConsultaComentariosPM){
        
        InputConsultaComentariosPM input_ws = (InputConsultaComentariosPM) Constantes.UTILS.strJsonToClass(InputConsultaComentariosPM, InputConsultaComentariosPM.class);
        OutputConsultaComentariosPM output_ws = new OutputConsultaComentariosPM();
        
        try{
            String Consulta = "";
            switch(input_ws.getTipo_comentarios()){
            case Constantes.COMENTARIOS_ACTIVIDAD: 
                Consulta = Constantes.QR_GET_COMENTARIOS_ACTIVIDAD.replaceAll("id_Actividad", input_ws.getId_deconsulta());
            break;
            
            case Constantes.COMENTARIOS_PUNTA: 
                Consulta = Constantes.QR_GET_COMENTARIOS_PUNTA.replaceAll("id_cs", input_ws.getId_deconsulta());
            break;
            }
            
            String[][] arr_coments = Constantes.CRUD_FFM.consultaBD(Consulta);
            
            if(arr_coments.length >0){
                Comentarios[] arr_comen = new Comentarios[arr_coments.length];
                for(int row = 0; row < arr_coments.length; row++){
                    Comentarios coment = new Comentarios();
                    
                    coment.setId_comentario(arr_coments[row][0]);
                    coment.setComentario(arr_coments[row][1]);
                    coment.setFecha(arr_coments[row][2]);
                    coment.setUsuario_comenta(arr_coments[row][3]);
                    
                    arr_comen[row] = coment;                    
                }
                output_ws.setResult(Constantes.RESULT_SUSSES);
                output_ws.setResultDescripcion(Constantes.RESULTSESCRIPTION_SUSSES);
                output_ws.setVersion(Constantes.WS_VERSION);
                output_ws.setComentarios(arr_comen);
            }else{
                output_ws.setResult(Constantes.RESULT_SUSSES);
                output_ws.setResultDescripcion("No se encontraron datos");
                output_ws.setVersion(Constantes.WS_VERSION);    
            }
        
        }catch(Exception ex){
            output_ws.setResult(Constantes.RESULT_ERROR);
            output_ws.setResultDescripcion("ERROR : " + ex.toString());
            output_ws.setVersion(Constantes.WS_VERSION);
        }catch (Throwable exs) {
            output_ws.setResult(Constantes.RESULT_ERROR);
            output_ws.setResultDescripcion("ERROR : " + exs.toString());
            output_ws.setVersion(Constantes.WS_VERSION);
        }
        
        System.out.println(Constantes.UTILS.classTostrJson(output_ws)); 
        
        return Response.status(200).entity(Constantes.UTILS.classTostrJson(output_ws)).build();
    }
}
