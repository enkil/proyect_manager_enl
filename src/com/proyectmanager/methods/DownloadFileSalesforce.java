package com.proyectmanager.methods;

import com.proyectmanager.inputs.InputDownloadFileSalesforce;
import com.proyectmanager.outputs.OutputDownloadFileSalesforce;
import com.proyectmanager.objects.Archivos;
import com.proyectmanager.objects.Constantes;

import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.sobject.SObject;

import javax.ws.rs.core.Response;

public class DownloadFileSalesforce {
    
    public DownloadFileSalesforce() {
        super();
        Constantes.UTILS.loadConfigIN_DB();
    }
    
    public Response downloadFileSalesforce(String InputDownloadFileSalesforce){
        InputDownloadFileSalesforce input_ws = (InputDownloadFileSalesforce) Constantes.UTILS.strJsonToClass(InputDownloadFileSalesforce, InputDownloadFileSalesforce.class);
        OutputDownloadFileSalesforce output_ws = new OutputDownloadFileSalesforce();
        
        try{
            
            System.out.println("consulta : " + Constantes.QR_CONSULTA_ARCHIVO_SF.replaceAll("id_content_version", input_ws.getId_file_sf()));
            
            QueryResult files_in_sf = Constantes.CRUD_SF.query_sf(Constantes.QR_CONSULTA_ARCHIVO_SF.replaceAll("id_content_version", input_ws.getId_file_sf()));
            
            if(files_in_sf.getDone()){
                if(files_in_sf.getSize() > 0){
                    for(SObject img_sf : files_in_sf.getRecords()){
                        Archivos archivos = new Archivos();
                        archivos.setId_archivo_sf(input_ws.getId_file_sf());
                        archivos.setNombre_archivo((String) img_sf.getField("Title"));
                        archivos.setExtencion_archivo((String) img_sf.getField("FileExtension"));
                        archivos.setBody_archivo((String) img_sf.getField("VersionData"));    
                        output_ws.setResult(Constantes.RESULT_SUSSES);
                        output_ws.setResultDescripcion(Constantes.RESULTSESCRIPTION_SUSSES);    
                        output_ws.setVersion(Constantes.WS_VERSION);
                        output_ws.setArchivos(archivos);
                    }
                }else{
                    output_ws.setResult(Constantes.RESULT_SUSSES);
                    output_ws.setResultDescripcion("No se encontraron datos");    
                    output_ws.setVersion(Constantes.WS_VERSION);
                }
            }else{
                output_ws.setResult(Constantes.RESULT_ERROR);
                output_ws.setResultDescripcion("ERROR QR_SF : " + files_in_sf.getQueryLocator());    
                output_ws.setVersion(Constantes.WS_VERSION);
            }
        
        }catch(Exception ex){
            output_ws.setResult(Constantes.RESULT_ERROR);
            output_ws.setResultDescripcion("ERROR : " + ex.toString());    
            output_ws.setVersion(Constantes.WS_VERSION);
        }catch (Throwable exs) {
            output_ws.setResult(Constantes.RESULT_ERROR);
            output_ws.setResultDescripcion("ERROR : " + exs.toString());
            output_ws.setVersion(Constantes.WS_VERSION);
        }
        
        System.out.println(Constantes.UTILS.classTostrJson(output_ws));
        
        return Response.status(200).entity(Constantes.UTILS.classTostrJson(output_ws)).build();
        
    }
}
