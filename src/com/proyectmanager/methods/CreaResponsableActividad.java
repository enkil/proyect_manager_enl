package com.proyectmanager.methods;

import com.proyectmanager.inputs.InputCreaResponsableActividad;

import com.proyectmanager.objects.Constantes;

import com.proyectmanager.outputs.OutputCreaResponsableActividad;

import javax.ws.rs.core.Response;

public class CreaResponsableActividad {
    public CreaResponsableActividad() {
        super();
        Constantes.UTILS.loadConfigIN_DB();
    }
    
    public Response creaResponsableActividad(String InputCreaResponsableActividad){
        InputCreaResponsableActividad input_ws = (InputCreaResponsableActividad) Constantes.UTILS.strJsonToClass(InputCreaResponsableActividad, InputCreaResponsableActividad.class);
        OutputCreaResponsableActividad output_ws = new OutputCreaResponsableActividad();
        
        try{
            
            String id_responsable = Constantes.CRUD_FFM.consultaBD(Constantes.QR_GET_ID_RESPONSABLE)[0][0];
            String key_instert = input_ws.getNombre() + input_ws.getApellido_Paterno() + input_ws.getApellido_Materno()+input_ws.getTelefono();
            Boolean registro = Constantes.CRUD_FFM.insertaDB(Constantes.QR_SET_NEW_RESPONSABLE.replaceAll("cadena_concat", key_instert).replaceAll("id_respo", id_responsable)
                                .replaceAll("nombre_res", input_ws.getNombre()).replaceAll("apepa_res", input_ws.getApellido_Paterno()).replaceAll("apema_res", input_ws.getApellido_Materno()).replaceAll("tel", input_ws.getTelefono()));
            
            if(registro){
                output_ws.setResult(Constantes.RESULT_SUSSES);
                output_ws.setResultDescripcion(Constantes.RESULTSESCRIPTION_SUSSES);
                output_ws.setVersion(Constantes.WS_VERSION);
                output_ws.setId_responsable(id_responsable);
            }else{
                output_ws.setResult(Constantes.RESULT_ERROR);
                output_ws.setResultDescripcion("No se pudo insertar el nuevo responsable");
                output_ws.setVersion(Constantes.WS_VERSION);    
            }
                
        }catch(Exception ex){
            output_ws.setResult(Constantes.RESULT_ERROR);
            output_ws.setResultDescripcion("ERROR : " + ex.toString());
            output_ws.setVersion(Constantes.WS_VERSION);    
        }catch(Throwable tx){
            System.err.println("Error : " + tx.toString());
            output_ws.setResult(Constantes.RESULT_ERROR);
            output_ws.setResultDescripcion("ERROR : " + tx.toString());
            output_ws.setVersion(Constantes.WS_VERSION);
        }
        
        System.out.println(Constantes.UTILS.classTostrJson(output_ws));
        
        return Response.status(200).entity(Constantes.UTILS.classTostrJson(output_ws)).build();
        
    }
}
