package com.proyectmanager.methods;

import com.proyectmanager.inputs.InputCatalogosActividadesPM;
import com.proyectmanager.outputs.OutputCatalogosActividadesPM;
import com.proyectmanager.objects.Actividad;
import com.proyectmanager.objects.Archivos;
import com.proyectmanager.objects.Constantes;
import com.proyectmanager.objects.Responsables;

import javax.ws.rs.core.Response;

public class CatalogosActividadesPM {
    public CatalogosActividadesPM() {
        super();
    }
    
    public Response catalogosActividadesPM(String InputCatalogosActividadesPM){
        
        InputCatalogosActividadesPM input_ws = (InputCatalogosActividadesPM) Constantes.UTILS.strJsonToClass(InputCatalogosActividadesPM, InputCatalogosActividadesPM.class);
        OutputCatalogosActividadesPM output_ws = new OutputCatalogosActividadesPM();
        
        try{
            
            String[][] responsables_db = Constantes.CRUD_FFM.consultaBD(Constantes.QR_GET_RESPONSABLES);
            String[][] actividades_db = Constantes.CRUD_FFM.consultaBD(Constantes.QR_GET_ACTIVIDADES_TERCER_NIVEL);
            String[][] archivos_db = Constantes.CRUD_FFM.consultaBD(Constantes.QR_GET_LISTADOCUMENTOS);
            
            Actividad[] arr_actividades = null;
            Responsables[] arr_responsables = null;
            Archivos[] arr_archivos = null;
            
            if(responsables_db.length > 0){
                arr_responsables = new Responsables[responsables_db.length];            
                for(int row = 0; row < responsables_db.length;row++){
                    Responsables responsable = new Responsables();
                    responsable.setId_Responsable(responsables_db[row][0]);
                    responsable.setNombre_completo(responsables_db[row][1]);
                    responsable.setNombre(responsables_db[row][2]);
                    responsable.setApellidoPaterno(responsables_db[row][3]);
                    responsable.setApellidoMaterno(responsables_db[row][4]);
                    responsable.setNum_Telefonico(responsables_db[row][5]);
                    arr_responsables[row] = responsable; 
                }                
            }
            
            if(actividades_db.length > 0){
                arr_actividades = new Actividad[actividades_db.length];
                for(int row = 0; row < actividades_db.length;row++){
                    Actividad actividad = new Actividad();
                    actividad.setId_actividad(actividades_db[row][0]);
                    actividad.setNombre_actividad(actividades_db[row][1]);
                    arr_actividades[row] = actividad;
                }                
            }
            
            if(archivos_db.length > 0){
                arr_archivos = new Archivos[archivos_db.length];            
                for(int row = 0; row < archivos_db.length;row++){
                    Archivos archivos = new Archivos();
                    archivos.setId_archivo((String) archivos_db[row][0]);
                    archivos.setDescripcion((String) archivos_db[row][1]);
                    archivos.setIcono((String) archivos_db[row][2]);
                    arr_archivos[row] = archivos; 
                }                
            }
            
            output_ws.setResult(Constantes.RESULT_SUSSES); 
            output_ws.setResultDescripcion(Constantes.RESULTSESCRIPTION_SUSSES);
            output_ws.setVersion(Constantes.WS_VERSION);
            output_ws.setActividades(arr_actividades);
            output_ws.setResponsables(arr_responsables);
            output_ws.setArchivos(arr_archivos);
        
        }catch(Exception ex){
           output_ws.setResult(Constantes.RESULT_ERROR); 
           output_ws.setResultDescripcion("ERROR : " + ex.toString());
           output_ws.setVersion(Constantes.WS_VERSION);
        }catch (Throwable exs) {
            output_ws.setResult(Constantes.RESULT_ERROR);
            output_ws.setResultDescripcion("ERROR : " + exs.toString());
            output_ws.setVersion(Constantes.WS_VERSION);
        }
        
        System.out.println(Constantes.UTILS.classTostrJson(output_ws));
        
        return Response.status(200).entity(Constantes.UTILS.classTostrJson(output_ws)).build();
    }
    
}
