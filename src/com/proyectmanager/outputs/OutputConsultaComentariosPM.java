package com.proyectmanager.outputs;

import com.proyectmanager.objects.Comentarios;


public class OutputConsultaComentariosPM {

    private String result;
    private String resultDescripcion;
    private String Version;
    private Comentarios[] Comentarios;

    public OutputConsultaComentariosPM() {
        super();
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public void setResultDescripcion(String resultDescripcion) {
        this.resultDescripcion = resultDescripcion;
    }

    public String getResultDescripcion() {
        return resultDescripcion;
    }

    public void setVersion(String Version) {
        this.Version = Version;
    }

    public String getVersion() {
        return Version;
    }

    public void setComentarios(Comentarios[] Comentarios) {
        this.Comentarios = Comentarios;
    }

    public Comentarios[] getComentarios() {
        return Comentarios;
    }
}
