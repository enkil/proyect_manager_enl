package com.proyectmanager.outputs;

import com.proyectmanager.objects.Archivos;

public class OutputConsultaListaArchivosPM {

    private String result;
    private String resultDescripcion;
    private String Version;
    private Archivos[] Archivos;

    public OutputConsultaListaArchivosPM() {
        super();
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public void setResultDescripcion(String resultDescripcion) {
        this.resultDescripcion = resultDescripcion;
    }

    public String getResultDescripcion() {
        return resultDescripcion;
    }

    public void setVersion(String Version) {
        this.Version = Version;
    }

    public String getVersion() {
        return Version;
    }

    public void setArchivos(Archivos[] Archivos) {
        this.Archivos = Archivos;
    }

    public Archivos[] getArchivos() {
        return Archivos;
    }
}
