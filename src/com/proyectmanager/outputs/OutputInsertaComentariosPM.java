package com.proyectmanager.outputs;

public class OutputInsertaComentariosPM {

    private String result;
    private String resultDescripcion;
    private String Version;

    public OutputInsertaComentariosPM() {
        super();
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public void setResultDescripcion(String resultDescripcion) {
        this.resultDescripcion = resultDescripcion;
    }

    public String getResultDescripcion() {
        return resultDescripcion;
    }

    public void setVersion(String Version) {
        this.Version = Version;
    }

    public String getVersion() {
        return Version;
    }
}
