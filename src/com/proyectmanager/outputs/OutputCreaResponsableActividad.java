package com.proyectmanager.outputs;

public class OutputCreaResponsableActividad {
    
    private String result;
    private String resultDescripcion;
    private String Version;
    private String Id_responsable;
    
    public OutputCreaResponsableActividad() {
        super();
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public void setResultDescripcion(String resultDescripcion) {
        this.resultDescripcion = resultDescripcion;
    }

    public String getResultDescripcion() {
        return resultDescripcion;
    }

    public void setVersion(String Version) {
        this.Version = Version;
    }

    public String getVersion() {
        return Version;
    }

    public void setId_responsable(String Id_responsable) {
        this.Id_responsable = Id_responsable;
    }

    public String getId_responsable() {
        return Id_responsable;
    }
}
