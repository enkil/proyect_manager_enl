package com.proyectmanager.outputs;

import com.proyectmanager.objects.Actividad;
import com.proyectmanager.objects.Archivos;
import com.proyectmanager.objects.Responsables;


public class OutputCatalogosActividadesPM {

    private String result;
    private String resultDescripcion;
    private String Version;
    private Responsables[] Responsables;
    private Actividad[] Actividades;
    private Archivos[] Archivos;

    public OutputCatalogosActividadesPM() {
        super();
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public void setResultDescripcion(String resultDescripcion) {
        this.resultDescripcion = resultDescripcion;
    }

    public String getResultDescripcion() {
        return resultDescripcion;
    }

    public void setVersion(String Version) {
        this.Version = Version;
    }

    public String getVersion() {
        return Version;
    }

    public void setResponsables(Responsables[] Responsables) {
        this.Responsables = Responsables;
    }

    public Responsables[] getResponsables() {
        return Responsables;
    }

    public void setActividades(Actividad[] Actividades) {
        this.Actividades = Actividades;
    }

    public Actividad[] getActividades() {
        return Actividades;
    }

    public void setArchivos(Archivos[] Archivos) {
        this.Archivos = Archivos;
    }

    public Archivos[] getArchivos() {
        return Archivos;
    }

}
