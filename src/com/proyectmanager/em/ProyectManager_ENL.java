package com.proyectmanager.em;

import com.proyectmanager.methods.CatalogosActividadesPM;
import com.proyectmanager.methods.ConsultaComentariosPM;
import com.proyectmanager.methods.ConsultaListaArchivosPM;
import com.proyectmanager.methods.CreaResponsableActividad;
import com.proyectmanager.methods.DownloadFileSalesforce;
import com.proyectmanager.methods.InsertaComentariosPM;
import com.proyectmanager.methods.UploadFileSalesforce;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("ProyectManager_TPE")
public class ProyectManager_ENL {
    
    
    
    public ProyectManager_ENL() {
        super();
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Path("CatalogoActividadesPM")
    public Response catalogosActividadesPM(String InputCatalogosActividades) {
        return new CatalogosActividadesPM().catalogosActividadesPM(InputCatalogosActividades);
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Path("InsertaComentariosPM")
    public Response insertaComentariosPM(String InputInsertaComentariosPM) {
        return new InsertaComentariosPM().insertaComentariosPM(InputInsertaComentariosPM);
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Path("ConsultaComentariosPM")
    public Response consultaComentariosPM(String InputConsultaComentariosPM) {
        return new ConsultaComentariosPM().consultaComentariosPM(InputConsultaComentariosPM);
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Path("ConsultaListaArchivosPM")
    public Response consultaListaArchivosPM(String InputConsultaArchivosPM) {
        return new ConsultaListaArchivosPM().consultaListaArchivosPM(InputConsultaArchivosPM);
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Path("UploadFilesPM")
    public Response uploadFileSalesforce(String InputUploadFileSalesforce) {
        return new UploadFileSalesforce().uploadFileSalesforce(InputUploadFileSalesforce);
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Path("DownloadFilesPM")
    public Response downloadFileSalesforce(String InputDownloadFileSalesforce) {
        return new DownloadFileSalesforce().downloadFileSalesforce(InputDownloadFileSalesforce);
    }
    
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Path("CreaNuevoResponsablePM")
    public Response creaResponsableActividad(String InputCreaResponsableActividad){
        return new CreaResponsableActividad().creaResponsableActividad(InputCreaResponsableActividad);    
    }

}
